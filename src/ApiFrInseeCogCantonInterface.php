<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogCantonInterface interface file.
 * 
 * This represents the cantons from the insee database.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogCantonInterface extends Stringable
{
	
	/**
	 * Gets the id of this canton.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the fk of the related arrondissement.
	 * 
	 * @return string
	 */
	public function getFkArrondissement() : string;
	
	/**
	 * Gets the fk of the related type of canton.
	 * 
	 * @return string
	 */
	public function getFkTypeCanton() : string;
	
	/**
	 * Gets the fk of the related composition cantonale.
	 * 
	 * @return int
	 */
	public function getFkCompositionCantonale() : int;
	
	/**
	 * Gets the fk of the related commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string;
	
	/**
	 * Gets the type of name of this canton.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int;
	
	/**
	 * Gets the name of this canton.
	 * 
	 * @return string
	 */
	public function getNcc() : string;
	
	/**
	 * Gets the enriched name of this canton.
	 * 
	 * @return string
	 */
	public function getNccenr() : string;
	
}
