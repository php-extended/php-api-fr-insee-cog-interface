<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogCollectiviteTerritorialeInterface interface file.
 * 
 * This represents all the _collectivites territoriales ayant competences
 * departementales_ .
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrInseeCogCollectiviteTerritorialeInterface extends Stringable
{
	
	/**
	 * Gets the id of this collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the fk of the related region.
	 * 
	 * @return string
	 */
	public function getFkRegion() : string;
	
	/**
	 * Gets the fk of the related commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string;
	
	/**
	 * Gets the fk of the type of name of this collectivite territoriale.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int;
	
	/**
	 * Gets the name of this  collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getNcc() : string;
	
	/**
	 * Gets the enriched name of this  collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getNccenr() : string;
	
	/**
	 * Gets the libelle of this collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
