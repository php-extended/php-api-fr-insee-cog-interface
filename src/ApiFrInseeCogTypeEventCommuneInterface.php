<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogTypeEventCommuneInterface interface file.
 * 
 * This class represents the reason for an event on a commune.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogTypeEventCommuneInterface extends Stringable
{
	
	/**
	 * Gets the id of this type event.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the libelle of this type event.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
