<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogTypeCantonInterface interface file.
 * 
 * This class represents the type of canton.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogTypeCantonInterface extends Stringable
{
	
	/**
	 * Gets the id of this type canton.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the libelle of this type canton.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
