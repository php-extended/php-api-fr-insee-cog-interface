<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogTypeCommuneInterface interface file.
 * 
 * The type of commune replaces the old "actual" code but differs from it in
 * that it does not represents the same state of a commune, but different
 * communes altogether. There may exist a commune and a delegated commune with
 * the same code.
 * 
 * Since 2022, the type commune also includes the zoning type that is used with
 * the collectivities on sea.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogTypeCommuneInterface extends Stringable
{
	
	/**
	 * Gets the id of this type commune.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the libelle of this type commune.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
