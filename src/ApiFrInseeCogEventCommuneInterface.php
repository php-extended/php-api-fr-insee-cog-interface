<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrInseeCogEventCommuneInterface interface file.
 * 
 * This represents the events on the communes from the insee database.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogEventCommuneInterface extends Stringable
{
	
	/**
	 * Gets the date of the event.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateEffet() : DateTimeInterface;
	
	/**
	 * Gets the id of the related type of event.
	 * 
	 * @return int
	 */
	public function getFkTypeEventCommune() : int;
	
	/**
	 * Gets the id of the type of commune before the event.
	 * 
	 * @return string
	 */
	public function getFkTypeCommuneBefore() : string;
	
	/**
	 * Gets the id of the commune before the event.
	 * 
	 * @return string
	 */
	public function getFkCommuneBefore() : string;
	
	/**
	 * Gets the id of the type of commune after the event.
	 * 
	 * @return string
	 */
	public function getFkTypeCommuneAfter() : string;
	
	/**
	 * Gets the id of the commune after the event.
	 * 
	 * @return string
	 */
	public function getFkCommuneAfter() : string;
	
	/**
	 * Gets the type of name of the commune before the event.
	 * 
	 * @return int
	 */
	public function getFkTnccBefore() : int;
	
	/**
	 * Gets the name of the commune before the event.
	 * 
	 * @return string
	 */
	public function getNccBefore() : string;
	
	/**
	 * Gets the enriched name of the commune before the event.
	 * 
	 * @return string
	 */
	public function getNccenrBefore() : string;
	
	/**
	 * Gets the type of name of the commune after the event.
	 * 
	 * @return int
	 */
	public function getFkTnccAfter() : int;
	
	/**
	 * Gets the name of the commune after the event.
	 * 
	 * @return string
	 */
	public function getNccAfter() : string;
	
	/**
	 * Gets the enriched name of the commune after the event.
	 * 
	 * @return string
	 */
	public function getNccenrAfter() : string;
	
}
