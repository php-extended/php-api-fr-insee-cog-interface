<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrInseeCogPaysHistoryInterface interface file.
 * 
 * This represents the events on the countries from the insee database.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogPaysHistoryInterface extends Stringable
{
	
	/**
	 * Gets the id of the related country.
	 * 
	 * @return int
	 */
	public function getFkPaysId() : int;
	
	/**
	 * Gets the id of the related country before this record exists.
	 * 
	 * @return ?int
	 */
	public function getFkPaysBeforeId() : ?int;
	
	/**
	 * Gets the name of this country used in the COG.
	 * 
	 * @return string
	 */
	public function getLibCog() : string;
	
	/**
	 * Gets official name of this country.
	 * 
	 * @return string
	 */
	public function getLibEnr() : string;
	
	/**
	 * Gets the start date of validity of this country.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateStart() : DateTimeInterface;
	
	/**
	 * Gets the end date of validity of this country.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateEnd() : ?DateTimeInterface;
	
}
