<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogCommuneInterface interface file.
 * 
 * This represents the communes from the insee database.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogCommuneInterface extends Stringable
{
	
	/**
	 * Gets the id of this commune.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the fk of the related type commune.
	 * 
	 * @return string
	 */
	public function getFkTypeCommune() : string;
	
	/**
	 * Gets the fk of the related departement.
	 * 
	 * @return string
	 */
	public function getFkDepartement() : string;
	
	/**
	 * Gets the fk of the related parent commune, if any.
	 * 
	 * @return ?string
	 */
	public function getFkCommuneParent() : ?string;
	
	/**
	 * Gets the fk of the type of name of this commune.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int;
	
	/**
	 * Gets the name of this commune.
	 * 
	 * @return string
	 */
	public function getNcc() : string;
	
	/**
	 * Gets the enriched name of this commune.
	 * 
	 * @return string
	 */
	public function getNccenr() : string;
	
}
