<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogTnccInterface interface file.
 * 
 * The type of name in clear : this variable is to be able to write the
 * complete name of the commune for a label with article and add link
 * (charnière) if the name is used in an expression like « la commune de
 * Marseille », « l'arrondissement du Mans », etc.
 * 
 * For names of communes (and cantons and arrondissements), the article is
 * mandatory. ("Rochelle" does not exist without article => "La Rochelle").
 * This is not the case for department or region names (like
 * "Charente-Maritime" without article). In that case, this code is only used
 * for its link (charnière).
 * 
 * Exemple for a commune :
 * Com = 104 | Dep = 66 | NCC = MASOS | TNCC = 8 | charnière = DE LOS |
 * article = LOS
 * 
 * Name of the commune = LOS MASOS
 * Should be written as : "Commune DE LOS MASOS" // "commune de Los Masos"
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogTnccInterface extends Stringable
{
	
	/**
	 * Gets the id of type of name.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the article of the noun.
	 * 
	 * @return string
	 */
	public function getArticle() : string;
	
	/**
	 * Gets the charniere of the noun.
	 * 
	 * @return string
	 */
	public function getCharniere() : string;
	
	/**
	 * Gets the space before the noun.
	 * 
	 * @return string
	 */
	public function getEspace() : string;
	
}
