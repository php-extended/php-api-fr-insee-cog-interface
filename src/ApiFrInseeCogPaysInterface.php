<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogPaysInterface interface file.
 * 
 * This represents the countries from the insee database.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogPaysInterface extends Stringable
{
	
	/**
	 * Gets the id of this country.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the fk of the related actual country.
	 * 
	 * @return int
	 */
	public function getFkActualitePays() : int;
	
	/**
	 * Gets the fk of the current parent country, if any.
	 * 
	 * @return ?int
	 */
	public function getFkPaysParent() : ?int;
	
	/**
	 * Gets the year of creation of this country, if after 1943.
	 * 
	 * @return ?int
	 */
	public function getCreationYear() : ?int;
	
	/**
	 * Gets the name of this country used in the COG.
	 * 
	 * @return string
	 */
	public function getLibCog() : string;
	
	/**
	 * Gets official name of this country.
	 * 
	 * @return string
	 */
	public function getLibEnr() : string;
	
	/**
	 * Gets the code iso bi-alpha, if any.
	 * 
	 * @return ?string
	 */
	public function getIso2() : ?string;
	
	/**
	 * Gets the code iso tri-alpha, if any.
	 * 
	 * @return ?string
	 */
	public function getIso3() : ?string;
	
	/**
	 * Gets the code iso tri-alphanumeric, if any.
	 * 
	 * @return ?string
	 */
	public function getIsonum3() : ?string;
	
}
