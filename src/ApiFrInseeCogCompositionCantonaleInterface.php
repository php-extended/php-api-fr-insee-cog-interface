<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogCompositionCantonaleInterface interface file.
 * 
 * This represents the state of fractions of communes present in the canton.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrInseeCogCompositionCantonaleInterface extends Stringable
{
	
	/**
	 * Gets the id of this composition cantonale.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the libelle of this composition cantonale.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
