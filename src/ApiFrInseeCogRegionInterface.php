<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Stringable;

/**
 * ApiFrInseeCogRegionInterface interface file.
 * 
 * This class represents the regions from the insee database.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogRegionInterface extends Stringable
{
	
	/**
	 * Gets the id of this region.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the fk of the related pays.
	 * 
	 * @return string
	 */
	public function getFkPays() : string;
	
	/**
	 * Gets the fk of the related commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string;
	
	/**
	 * Gets the fk of the type of name of this region.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int;
	
	/**
	 * Gets the name of this region.
	 * 
	 * @return string
	 */
	public function getNcc() : string;
	
	/**
	 * Gets the enriched name of this region.
	 * 
	 * @return string
	 */
	public function getNccenr() : string;
	
}
