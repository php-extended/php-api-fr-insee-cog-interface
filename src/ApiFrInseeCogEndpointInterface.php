<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use Iterator;
use Stringable;

/**
 * ApiFrInseeCogEndpointInterface interface file.
 * 
 * This class is the main access point to get objects about the files that
 * are stored in this repository.
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCogEndpointInterface extends Stringable
{
	
	/**
	 * Gets the minimum year where data is known and available.
	 * 
	 * @return integer
	 */
	public function getMinimumAvailableYear() : int;
	
	/**
	 * Gets the maximum year where data is known and available.
	 * 
	 * @return integer
	 */
	public function getMaximumAvailableYear() : int;
	
	/**
	 * Gets an iterator over all pays for the given year.
	 * 
	 * @return Iterator<integer, ApiFrInseeCogPaysInterface>
	 */
	public function getPaysIterator() : Iterator;

	/**
	 * Gets an iterator over all regions for the given year.
	 * 
	 * @return Iterator<integer, ApiFrInseeCogPaysHistoryInterface>
	 */
	public function getPaysHistoryIterator() : Iterator;
	
	/**
	 * Gets an iterator over all regions for the given year.
	 * 
	 * @param integer $year
	 * @return Iterator<integer, ApiFrInseeCogRegionInterface>
	 */
	public function getRegionIterator(int $year) : Iterator;
	
	/**
	 * Gets an iterator over all departements for the given year.
	 * 
	 * @param integer $year
	 * @return Iterator<integer, ApiFrInseeCogDepartementInterface>
	 */
	public function getDepartementIterator(int $year) : Iterator;
	
	/**
	 * Gets an iterator over all arrondissements for the given year.
	 * 
	 * @param integer $year
	 * @return Iterator<integer, ApiFrInseeCogArrondissementInterface>
	 */
	public function getArrondissementIterator(int $year) : Iterator;
	
	/**
	 * Gets an iterator over all cantons for the given year.
	 * 
	 * @param integer $year
	 * @return Iterator<integer, ApiFrInseeCogCantonInterface>
	 */
	public function getCantonIterator(int $year) : Iterator;

	/**
	 * Gets an iterator over all collectivites territoriales ayant competences departementales for the given year.
	 * 
	 * @param integer $year
	 * @return Iterator<integer, ApiFrInseeCogCollectiviteTerritorialeInterface>
	 */
	public function getCollectiviteTerritorialeIterator(int $year) : Iterator;
	
	/**
	 * Gets an iterator over all communes for the given year.
	 * 
	 * @param integer $year
	 * @return Iterator<integer, ApiFrInseeCogCommuneInterface>
	 */
	public function getCommuneIterator(int $year) : Iterator;
	
	/**
	 * Gets an iterator over all the event for communes.
	 * 
	 * @return Iterator<integer, ApiFrInseeCogEventCommuneInterface>
	 */
	public function getEventCommuneIterator() : Iterator;
	
	/**
	 * Gets an iterator over all actualites pays.
	 * 
	 * @return Iterator<integer, ApiFrInseeCogActualitePaysInterface>
	 */
	public function getActualitePaysIterator() : Iterator;
	
	/**
	 * Gets the actualite pays from the given id, null if not found.
	 * 
	 * @param ?integer $id
	 * @return ?ApiFrInseeCogActualitePaysInterface
	 */
	public function getActualitePays(?int $id) : ?ApiFrInseeCogActualitePaysInterface;
	
	/**
	 * Gets an iterator over all compositions cantonales.
	 * 
	 * @return Iterator<integer, ApiFrInseeCogCompositionCantonaleInterface>
	 */
	public function getCompositionCantonaleIterator() : Iterator;
	
	/**
	 * Gets the composition cantonale from the given id, null if not found.
	 * 
	 * @param ?integer $id
	 * @return ?ApiFrInseeCogCompositionCantonaleInterface
	 */
	public function getCompositionCantonale(?int $id) : ?ApiFrInseeCogCompositionCantonaleInterface;
	
	/**
	 * Gets an iterator over all the types of names.
	 * 
	 * @return Iterator<integer, ApiFrInseeCogTnccInterface>
	 */
	public function getTnccIterator() : Iterator;
	
	/**
	 * Gets the tncc from the given id, null if not found.
	 * 
	 * @param ?integer $id
	 * @return ?ApiFrInseeCogTnccInterface
	 */
	public function getTncc(?int $id) : ?ApiFrInseeCogTnccInterface;
	
	/**
	 * Gets an iterator over all the types of cantons.
	 * 
	 * @return Iterator<string, ApiFrInseeCogTypeCantonInterface>
	 */
	public function getTypeCantonIterator() : Iterator;
	
	/**
	 * Gets the type canton from the given id, null if not found.
	 * 
	 * @param ?string $id
	 * @return ?ApiFrInseeCogTypeCantonInterface
	 */
	public function getTypeCanton(?string $id) : ?ApiFrInseeCogTypeCantonInterface;
	
	/**
	 * Gets an iterator over all the types of communes.
	 * 
	 * @return Iterator<string, ApiFrInseeCogTypeCommuneInterface>
	 */
	public function getTypeCommuneIterator() : Iterator;
	
	/**
	 * Gets the type commune from the given id, null if not found.
	 * 
	 * @param ?string $id
	 * @return ?ApiFrInseeCogTypeCommuneInterface
	 */
	public function getTypeCommune(?string $id) : ?ApiFrInseeCogTypeCommuneInterface;
	
	/**
	 * Gets an iterator over all the type of event for communes.
	 *
	 * @return Iterator<integer, ApiFrInseeCogTypeEventCommuneInterface>
	 */
	public function getTypeEventCommuneIterator() : Iterator;
	
	/**
	 * Gets the type event commune from the given id, null if not found.
	 * 
	 * @param ?integer $id
	 * @return ?ApiFrInseeCogTypeEventCommuneInterface
	 */
	public function getTypeEventCommune(?int $id) : ?ApiFrInseeCogTypeEventCommuneInterface;
	
}
